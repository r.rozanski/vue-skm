import {markWord} from "../directives/markWord";

const MarkWordDirectivePlugin = {
	install(Vue) {
		Vue.directive("markWord", markWord);
	}
};

export default MarkWordDirectivePlugin;