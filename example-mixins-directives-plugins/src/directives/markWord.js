export const markWord = function (element, binding) {
	const wordToMark = binding.value;
	const originalText = element.innerText;
	const re = new RegExp(wordToMark, "gi");
	const textWithMarkedWord = originalText.replace(re, "<span class='marked-word'>" + wordToMark + "</span>");

	element.innerHTML = textWithMarkedWord;
};