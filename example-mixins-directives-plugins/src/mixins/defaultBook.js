export const defaultBook = {
	data: function () {
		return {
			genre: "sci-fi",
			isDescriptionVisible: false
		}
	},
	methods: {
		toggleDescription: function () {
			this.isDescriptionVisible = !this.isDescriptionVisible;
		}
	}
};