import Vue from 'vue'
import App from './App.vue'
import MarkWordDirectivePlugin from "./plugins/MarkWordDirectivePlugin";

Vue.config.productionTip = false;

Vue.use(MarkWordDirectivePlugin);

new Vue({
	render: h => h(App)
}).$mount('#app')
