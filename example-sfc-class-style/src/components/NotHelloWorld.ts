import { Component, Vue } from 'vue-property-decorator';

@Component
export default class NotHelloWorld extends Vue {
    public message: string;

    constructor() {
        super();

        this.message = "Definitely NOT a 'Hello World' message!";
    }
}