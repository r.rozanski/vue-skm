import Vue from 'vue'
import Router from 'vue-router'
import TransitionExample from './views/TransitionExample.vue'
import EmptyPage from './views/EmptyPage.vue'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'example',
      component: TransitionExample
    },
    {
      path: '/empty',
      name: 'empty',
      component: EmptyPage
    }
  ]
})
